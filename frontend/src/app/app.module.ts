import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgregarPersonaComponent } from './agregar-persona/agregar-persona.component';
import { ListaPersonasComponent } from './lista-personas/lista-personas.component';
import { DatosUsuarioProviderService } from './datos-usuario-provider.service';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ProgressMaskComponent } from './progress-mask/progress-mask.component';

@NgModule({
  declarations: [
    AppComponent,
    AgregarPersonaComponent,
    ListaPersonasComponent,
    ProgressMaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DatosUsuarioProviderService,HttpClient,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
