import { Component, OnInit } from '@angular/core';
import { DatosUsuarioProviderService } from '../datos-usuario-provider.service';

@Component({
  selector: 'app-lista-personas',
  templateUrl: './lista-personas.component.html',
  styleUrls: ['./lista-personas.component.css']
})
export class ListaPersonasComponent implements OnInit {

  public resultado:any;
  public isLoading:boolean=true;
  public errorMessage:boolean=false;

  constructor(public datosUsuarioProvider: DatosUsuarioProviderService) { }

  ngOnInit() {
    this.obtenerlista();
  }

  public obtenerlista() {

    this.datosUsuarioProvider.lista().then(resultado => {
      this.resultado = resultado;
      this.isLoading=false;
      this.errorMessage=false;
    }).catch(e=>{
      this.isLoading=false;
      this.errorMessage = true;
      console.error(e);
    });
  }

}
