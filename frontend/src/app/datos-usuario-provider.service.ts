import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatosUsuarioProviderService {

  public serverURL: string = 'http://localhost:8080';

  constructor(public http: HttpClient) {

    this.log('DatosUsuarioProviderService Cargado');
  }

  public lista() {
    return this.GetQueryAPI("/persona-rest/api/v1/lista");
  }

  public agregar(params) {
    return this.PostQueryAPI(params, "/persona-rest/api/v1/agregar");
  }

  private PostQueryAPI(params, path) {
    var urlApi = this.serverURL + this.getPartialEndpoint(path);

    let prmAGLTask = new Promise((resolve, reject) => {
      this.http.post<DataResponse>(urlApi, JSON.stringify(params), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'accept': 'application/json'
        })
      }).subscribe(data => {
        this.log('Capturando respuesta desde REST');
        this.log(JSON.stringify(data));
        resolve(data);
      }, err => {
        let error = JSON.stringify(err);
        this.log('Ocurrio un error al consumir el servicio REST');
        this.log(error);
        reject(err);
      });
    });
    return prmAGLTask;
  }

  private GetQueryAPI(path) {
    var urlApi = this.serverURL + this.getPartialEndpoint(path);
    this.log('(GET) Conectandse a ' + urlApi);
    let prmAGLTask = new Promise((resolve, reject) => {
      this.log('Consumiendo servicios REST...' + path);
      this.http.get<DataResponse>(urlApi, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'accept': 'application/json',
          'Access-Control-Allow-Origin':'localhost:4200'
        })
      }).subscribe(data => {
        this.log('Capturando respuesta desde REST');
        this.log(JSON.stringify(data));
        resolve(data);
      }, err => {
        let error = JSON.stringify(err);
        this.log('Ocurrio un error al consumir el servicio REST');
        this.log(error);
        reject(err);
      });
    });
    return prmAGLTask;
  }

  public getPartialEndpoint(url: string): string {
    return url.replace(this.serverURL, '');
  }

  private log(message: string) {
    console.log(message);
  }
}

interface DataResponse {
  resultados: string[];
}

