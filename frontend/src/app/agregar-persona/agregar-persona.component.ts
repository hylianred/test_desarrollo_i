import { Component, OnInit } from '@angular/core';
import { DatosUsuarioProviderService } from '../datos-usuario-provider.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {

  public nombreCompleto;
  public fecha;

  constructor(
    public datosUsuarioProvider: DatosUsuarioProviderService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
  }

  agregar() {

    if (this.nombreCompleto == null || this.fecha==null) {
      alert("Faltan datos");
    } else {
      this.datosUsuarioProvider.agregar({
        "nombreCompleto": this.nombreCompleto,
        "fecha": this.fecha.replace(/\//g,"-")
      }).then(resultado => {
        alert("Usuario agregado con exito");
        window.location.reload();
      }).catch(e => {
        alert("Error:" + e.message);
        console.error(e);
      });;
    }
  }

}
