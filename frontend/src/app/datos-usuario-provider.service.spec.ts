import { TestBed } from '@angular/core/testing';

import { DatosUsuarioProviderService } from './datos-usuario-provider.service';

describe('DatosUsuarioProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatosUsuarioProviderService = TestBed.get(DatosUsuarioProviderService);
    expect(service).toBeTruthy();
  });
});
