# Instrucciones de instalación

## Requisitos

-Java 1.8
-Maven 3.5
-npm 6.9
-Angular 8 (Frontend)

## Compilación

En el directorio raìz, compilar con la siguiente instrucción:

`$ mvn clean install`

Una vez compilado, se generará un archivo JAR en el directorio `DatosPersonaRest/target`.

## Ejecución

### Si usas Linux, Mac u otros sistema con Unix, ejecutar:

`$ java -jar DatosPersonaRest/target/DatosPersonaRest-1.0.jar`

Esto ejecutará la aplicación y la deployará en un tomcat9 embebido

El servicio REST se desplegará en 

`http://localhost:8080/persona-rest/api/v1`

y contiene dos metodos:

`http://localhost:8080/persona-rest/api/v1/lista` -> Lista de personas
`http://localhost:8080/persona-rest/api/v1/agregar`-> Agregar persona

Ahora para probar la aplicación, hay que desplegar el frontend, para eso
hay que ir a la carpeta frontend y ejecutar:

`$ npm install`

Y a continuación, si todo sale bien, se debe ejecutar:

`$ ng serve`

Con esto se deplegará un live server en la ruta:

`http://localhost:4200`

Con esa url ya se pueden hacer pruebas.

### Si usas Windows

Debes configurar la variable de entorno para la ejecución de JAVA y npm.

Abrir una consola DOS y ejecutar en la raíz del proyecto para obtener el archivo de entrada:

`java -jar DatosPersonaRest/target/DatosPersonaRest-1.0.jar`

y luego ir la carpeta frontend y ejecutar:

`npm install`

y luego:

`ng serve`

## Información importante

Toda la documentación de los servicios Rest están en http://localhost:8080/swagger-ui.html
siempre y cuando la aplicación esté deplegada

## Ayuda

Si tienes dudas no olvides enviarme un correo a h.arriagada.mendez@gmail.com 