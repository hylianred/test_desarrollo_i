/*
 * Author:  Hermann Arriagada
 * Created: 14-feb-2020
 */

DROP TABLE IF EXISTS persona;
 
CREATE TABLE persona (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  primer_nombre VARCHAR(255) NOT NULL,
  primer_apellido VARCHAR(255) NOT NULL,
  edad NUMBER NULL,
  fecha DATE NULL
);

