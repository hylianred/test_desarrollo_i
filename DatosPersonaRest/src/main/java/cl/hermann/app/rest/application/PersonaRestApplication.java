package cl.hermann.app.rest.application;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Hermann Arriagada Mendez
 */
@SpringBootApplication
@ComponentScan(basePackages= {"cl.hermann"})
@EnableJpaRepositories("cl.hermann.app.repository")
@EntityScan("cl.hermann.app.model.entity")
public class PersonaRestApplication extends SpringBootServletInitializer{

    //Se establece la aplicacion con Springboot
    public static void main(String[] args) {
        SpringApplicationBuilder app = new SpringApplicationBuilder(PersonaRestApplication.class)
                .logStartupInfo(false);
        app.application().setBannerMode(Banner.Mode.OFF);
        app.application().run(args);
    }
    
}
