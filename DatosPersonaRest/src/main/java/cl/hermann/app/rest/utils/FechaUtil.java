package cl.hermann.app.rest.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 *
 * @author Hermann Arriagada
 */
public class FechaUtil {

    public static Integer obtenerEdad(Date fecha) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        LocalDate fechaNacimiento = LocalDate.parse(formatter.format(fecha), fmt);
        LocalDate ahora = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento, ahora.minusDays(1));
        return periodo.getYears();
    }

    public static Integer obtenerDiasFaltantesCumple(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        long totalDias;
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.parse(formatter.format(fecha));
        LocalDate nextBDay = birthday.withYear(today.getYear());
        if (nextBDay.isBefore(today) || nextBDay.isEqual(today)) {
            nextBDay = nextBDay.plusYears(1);
        }

        Period p = Period.between(today, nextBDay);
        totalDias = ChronoUnit.DAYS.between(today, nextBDay);

        return (int) totalDias;
    }
}
