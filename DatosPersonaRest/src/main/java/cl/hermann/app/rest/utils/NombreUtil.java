package cl.hermann.app.rest.utils;

import cl.hermann.app.rest.api.v1.impl.DatosPersonaRestImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hermann Arriagada
 */
public class NombreUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatosPersonaRestImpl.class);

    public static String obtenerPrimerNombre(String nombreCompleto) {
        String primerNombre = null;
        try {
            int primerEspacio = nombreCompleto.indexOf(" ");
            primerNombre = nombreCompleto.substring(0, primerEspacio);
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.getMessage());
        }
        return primerNombre;
    }

    public static String obtenerNombreDespuesDelPrimero(String nombreCompleto) {
        String nombreDespuesDelPrimero = null;
        try {
            int primerEspacio = nombreCompleto.indexOf(" ");
            String primerNombre = nombreCompleto.substring(0, primerEspacio);
            nombreDespuesDelPrimero = nombreCompleto.substring(primerEspacio).trim();
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.getMessage());
        }
        return nombreDespuesDelPrimero;
    }

    public static String obtenerPrimerApellido(String nombreDespuesDelPrimero) {
        String primerApellido = null;
        try {
            String[] separado = nombreDespuesDelPrimero.replaceAll("  ", " ").split(" ");
            if (separado.length > 2) {
                primerApellido = separado[1];
            } else {
                primerApellido = separado[0];
            }
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.getMessage());
        }
        return primerApellido;
    }
}
