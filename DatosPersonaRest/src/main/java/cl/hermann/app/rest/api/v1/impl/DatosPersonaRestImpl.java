package cl.hermann.app.rest.api.v1.impl;

import cl.hermann.app.rest.vo.ErrorVO;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import cl.hermann.app.rest.vo.ResponseVO;
import cl.hermann.app.rest.api.v1.DatosPersonaRest;
import cl.hermann.app.rest.utils.NombreUtil;
import cl.hermann.app.rest.vo.DatosPersonaVO;
import cl.hermann.app.rest.vo.PersonaVO;
import cl.hermann.app.services.PersonaService;
import cl.hermann.app.model.entity.Persona;
import cl.hermann.app.rest.utils.FechaUtil;
import cl.hermann.app.rest.vo.ResponseAddVO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = {"/persona-rest/api/v1"}, consumes = {"application/json; charset=UTF-8"}, produces = {"application/json; charset=UTF-8"})
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class DatosPersonaRestImpl implements DatosPersonaRest, Serializable {

    private static final long serialVersionUID = 5635014009825558528L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DatosPersonaRestImpl.class);

    public static String URL_API = "https://www.poemist.com/api/v1/randompoems";

    @Autowired
    PersonaService personaService;

    @Override
    @RequestMapping(value = "/lista", method = RequestMethod.GET, consumes = {"*/*"}, produces = {"application/json; charset=UTF-8"})
    public ResponseEntity lista() {

        Iterable<Persona> lista = this.personaService.getList();
        List<DatosPersonaVO> listaDatosPersona = new ArrayList<>();

        try {
            lista.forEach(persona -> {
                Integer diasFaltantes = FechaUtil.obtenerDiasFaltantesCumple(persona.getFecha());
                String diasFaltantesOPoema = String.valueOf(diasFaltantes);
                if (diasFaltantes == 365) {
                    RestTemplate plantilla = new RestTemplate();
                    HttpHeaders headers = new HttpHeaders();
                    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                    HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

                    //Consumo del API Rest
                    ResponseEntity<String> result = plantilla.exchange(URL_API, HttpMethod.GET, entity, String.class);

                    //Generar Objeto JSON en base a la respuesta
                    JSONObject jsonObject = new JSONObject(result.getBody().replace("[", "").replace("]", ""));
                    String title = jsonObject.getString("title");
                    String content = jsonObject.getString("content");

                    diasFaltantesOPoema = "Felicitaciones por tu cumpleanios. Ahora un poema: \n'" + title + "'\n'" + content + "'";
                }
                DatosPersonaVO datosPersona = new DatosPersonaVO(
                        persona.getPrimerNombre(),
                        persona.getPrimerApellido(),
                        persona.getEdad(),
                        diasFaltantesOPoema,
                        persona.getFecha()
                );
                listaDatosPersona.add(datosPersona);
            });
            return new ResponseEntity(new ResponseVO(true, "Peticiòn exitosa", listaDatosPersona), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.toString());
            LOGGER.debug("Error: {}", e.toString(), e);
            return new ResponseEntity(new ErrorVO("No fue posible obtener la información"), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    @RequestMapping(value = "/agregar", method = RequestMethod.POST, consumes = {"application/json; charset=UTF-8"}, produces = {"application/json; charset=UTF-8"})
    public ResponseEntity agregar(@RequestBody PersonaVO personaVO) {

        try {
            if (personaVO == null) {
                return new ResponseEntity(new ErrorVO("Falta informacion de persona"), HttpStatus.PRECONDITION_REQUIRED);
            } else if (personaVO.getNombreCompleto() == null || (personaVO.getNombreCompleto() != null && "".equals(personaVO.getNombreCompleto().trim()))) {
                return new ResponseEntity(new ErrorVO("Falta nombre completo"), HttpStatus.PRECONDITION_REQUIRED);
            } else if (personaVO.getFecha() == null) {
                return new ResponseEntity(new ErrorVO("Falta fecha"), HttpStatus.PRECONDITION_REQUIRED);
            } else {
                Persona persona = new Persona();
                persona.setPrimerNombre(NombreUtil.obtenerPrimerNombre(personaVO.getNombreCompleto()));
                persona.setPrimerApellido(NombreUtil.obtenerPrimerApellido(NombreUtil.obtenerNombreDespuesDelPrimero(personaVO.getNombreCompleto())));
                persona.setEdad(FechaUtil.obtenerEdad(personaVO.getFecha()));
                persona.setFecha(personaVO.getFecha());

                if (this.personaService != null) {
                    this.personaService.create(persona);
                    return new ResponseEntity(new ResponseAddVO(true, "Persona agregada con exito"), HttpStatus.OK);
                }else{
                    return new ResponseEntity(new ErrorVO("Problemas de guardado"), HttpStatus.PRECONDITION_FAILED);
                
                }

                
            }
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.toString());
            LOGGER.debug("Error: {}", e.toString(), e);
            return new ResponseEntity(new ErrorVO("No fue posible guardar la información"), HttpStatus.EXPECTATION_FAILED);
        }
    }
}
