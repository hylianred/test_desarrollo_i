package cl.hermann.app.rest.api.v1;

import cl.hermann.app.rest.vo.ErrorVO;
import cl.hermann.app.rest.vo.PersonaVO;
import cl.hermann.app.rest.vo.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

@Api(value = "/api/v1/persona", consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8")
public interface DatosPersonaRest {

    @ApiOperation(value = "Entrega informacion de personas")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Obtención exitosa.", response = ResponseVO.class),
        @ApiResponse(code = 400, message = "La petición es inválida.", response = ErrorVO.class),
        @ApiResponse(code = 412, message = "Ocurrió un error de validación", response = ErrorVO.class),
        @ApiResponse(code = 422, message = "Entidad no procesada", response = ErrorVO.class),
        @ApiResponse(code = 500, message = "Error interno del servidor.", response = ErrorVO.class)
    })
    public ResponseEntity lista();
    
    @ApiOperation(value = "Agregar nueva persona")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Obtención exitosa.", response = ResponseVO.class),
        @ApiResponse(code = 400, message = "La petición es inválida.", response = ErrorVO.class),
        @ApiResponse(code = 412, message = "Ocurrió un error de validación", response = ErrorVO.class),
        @ApiResponse(code = 422, message = "Entidad no procesada", response = ErrorVO.class),
        @ApiResponse(code = 500, message = "Error interno del servidor.", response = ErrorVO.class)
    })
    public ResponseEntity agregar(@ApiParam(value = "persona", required = true) PersonaVO persona);
}
