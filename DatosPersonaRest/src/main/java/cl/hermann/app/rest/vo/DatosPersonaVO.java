package cl.hermann.app.rest.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Hermann Arriagada
 */
@ApiModel(value = "datosPersona")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosPersonaVO {

    private String primerNombre = null;
    private String primerApellido = null;
    private Number edad = null;
    private String diasFaltantesOPoema = null;
    private Date fecha = null;

    public DatosPersonaVO(String primerNombre, 
            String primerApellido, 
            Number edad, 
            String diasFaltantesOPoema,
            Date fecha) {
        this.primerNombre = StringUtils.trimToEmpty(primerNombre);
        this.primerApellido = StringUtils.trimToEmpty(primerApellido);
        this.edad = edad;
        this.diasFaltantesOPoema = diasFaltantesOPoema;
        this.fecha = fecha;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public Number getEdad() {
        return edad;
    }

    public void setEdad(Number edad) {
        this.edad = edad;
    }

    public String getDiasFaltantesOPoema() {
        return diasFaltantesOPoema;
    }

    public void setDiasFaltantesOPoema(String diasFaltantesOPoema) {
        this.diasFaltantesOPoema = diasFaltantesOPoema;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
