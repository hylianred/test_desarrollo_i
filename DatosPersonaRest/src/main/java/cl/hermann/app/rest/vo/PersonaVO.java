package cl.hermann.app.rest.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

@ApiModel(value = "persona")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonaVO extends BaseBean {

    private static final long serialVersionUID = 6314335557508817920L;

    private String nombreCompleto = null;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date fecha = null;

    public PersonaVO() {
        this.nombreCompleto = "";
        this.fecha = new Date();
    }
    
    public PersonaVO(String nombreCompleto, Date fecha) {
        this.nombreCompleto = StringUtils.trimToEmpty(nombreCompleto);
        this.fecha = fecha;
    }

    @ApiModelProperty(value = "Nombre completo", required = true, example = "John H. McKensy")
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    @ApiModelProperty(value = "Fecha", required = true, example = "14-02-1989")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}
