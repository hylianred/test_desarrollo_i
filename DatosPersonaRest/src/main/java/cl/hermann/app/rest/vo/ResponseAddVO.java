package cl.hermann.app.rest.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;

@ApiModel(value = "responseAdd")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseAddVO extends BaseBean {

    private static final long serialVersionUID = 6314335557508817920L;

    private boolean ok = true;
    private String mensaje = null;
    private List<DatosPersonaVO> listaPersonas = null;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date fecha = null;
    
    public ResponseAddVO(boolean ok, String mensaje) {
        this.ok = true;
        this.mensaje = mensaje;
        this.fecha = new Date();
    }

    @ApiModelProperty(value = "Estado del objeto", required = true, allowableValues = "true, false", example = "false")
    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @ApiModelProperty(value = "Mensaje de la respuesta", required = true, example = "Error desconocido")
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @ApiModelProperty(value = "Fecha del mensaje", required = true, example = "14-02-2020 00:00:01")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
