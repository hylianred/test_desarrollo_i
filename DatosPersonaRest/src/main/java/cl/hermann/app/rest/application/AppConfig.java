package cl.hermann.app.rest.application;
        
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cl.hermann")
public class AppConfig {

}
