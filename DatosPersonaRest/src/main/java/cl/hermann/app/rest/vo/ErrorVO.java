package cl.hermann.app.rest.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

@ApiModel(value = "error")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorVO extends BaseBean {

    private static final long serialVersionUID = 6314335557508817920L;

    private boolean ok = false;
    private String mensaje = null;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date date = null;

    public ErrorVO() {
        this.ok = false;
        this.mensaje = "Error desconocido";
        this.date = new Date();
    }

    public ErrorVO(String mensaje) {
        this.ok = false;
        this.mensaje = StringUtils.trimToEmpty(mensaje);
        this.date = new Date();
    }

    @ApiModelProperty(value = "Estado del objeto", required = true, allowableValues = "true, false", example = "false")
    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    @ApiModelProperty(value = "Mensaje de la respuesta", required = true, example = "Error desconocido")
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @ApiModelProperty(value = "Fecha del mensaje", required = true, example = "14-02-2020 00:00:01")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
