package cl.hermann.app.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Component
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        Docket api = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo());
        return api;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Persona Datos Rest")
                .description("Servicio para obtener datos de persona, "
                        + "ingresando el nombre completo y una fecha, "
                        + "este calcula los dias faltantes al cumpleanios "
                        + "y en caso de que sea dia de cumpleanios retorna "
                        + "un poema aleatorio")
                .version("1.0")
                .build();
    }
}
