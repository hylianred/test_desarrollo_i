package cl.hermann.app.rest.api.v1.impl;

import cl.hermann.app.rest.api.v1.DatosPersonaRest;
import cl.hermann.app.rest.vo.PersonaVO;
import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Hermann Arriagada
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class DatosPersonaRestImplTest {

    DatosPersonaRest rest = new DatosPersonaRestImpl();
    
    public DatosPersonaRestImplTest() {
    }

    @BeforeAll
    public static void setUpClass() throws Exception {
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
    }

    @BeforeEach
    public void setUp() throws Exception {
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    /**
     * Test of agregar method, of class DatosPersonaRestImpl.
     */
    @Test
    public void testAgregarWithNullPersona() {
        System.out.println("Test unitario para agregar persona nula");
        PersonaVO personaVO = null;
        ResponseEntity result = this.rest.agregar(personaVO);
        assertNotEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testAgregarPersona() {
        System.out.println("Test unitario para agregar persona");
        PersonaVO personaVO = new PersonaVO();
        personaVO.setNombreCompleto("Juan Soto");
        personaVO.setFecha(new Date());
        ResponseEntity result = this.rest.agregar(personaVO);
        assertNotEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testAgregarPersonaWithNullFecha() {
        System.out.println("Test unitario para agregar persona con fecha nula");
        PersonaVO personaVO = new PersonaVO();
        personaVO.setNombreCompleto("Juan Soto");
        personaVO.setFecha(null);
        ResponseEntity result = this.rest.agregar(personaVO);
        assertNotEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void testAgregarPersonaWithNullNombre() {
        System.out.println("Test unitario para agregar persona con fecha nula");
        PersonaVO personaVO = new PersonaVO();
        personaVO.setNombreCompleto(null);
        personaVO.setFecha(new Date());
        ResponseEntity result = this.rest.agregar(personaVO);
        assertNotEquals(HttpStatus.OK, result.getStatusCode());
    }

}
