package cl.hermann.app.services;

import cl.hermann.app.model.entity.Persona;
import java.util.Optional;

/**
 *
 * @author Hermann Arriagada
 */
public interface PersonaService {

    public Iterable<Persona> getList();

    public void create(Persona persona);

    public Optional<Persona> read(Integer id);

    public Persona update(Persona persona);

    public void delete(Persona persona);
}
