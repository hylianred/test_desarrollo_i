package cl.hermann.app.services.impl;

import cl.hermann.app.services.PersonaService;
import cl.hermann.app.model.entity.Persona;
import cl.hermann.app.repository.PersonaRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Hermann Arriagada
 */
@Component
public class PersonaServiceImpl implements PersonaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaServiceImpl.class);

    @Autowired
    public PersonaRepository personaRepository;

    @Override
    public Iterable<Persona> getList() {
        Iterable<Persona> personaList = null;
        try {
            LOGGER.info("Obteniendo lista de personas");
            personaList = this.personaRepository.findAll();
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.getMessage());
            LOGGER.debug("Error: {}", e.getMessage());
        }
        return personaList;
    }

    @Override
    public void create(Persona persona) {
        try {
            LOGGER.info("Guardando informacion de persona");
            if (persona != null) {
                this.personaRepository.save(persona);
            }else{
                LOGGER.info("No se pudo guardar la informacion");
            }
        } catch (Exception e) {
            LOGGER.error("Error: {}", e.getMessage());
            LOGGER.debug("Error: {}", e.getMessage());
        }
    }

    @Override
    public Optional<Persona> read(Integer id) {
        Optional<Persona> persona = null;
        persona = this.personaRepository.findById(id);

        return persona;
    }

    @Override
    public Persona update(Persona persona) {
        this.personaRepository.save(persona);
        return persona;
    }

    public void delete(Persona persona) {
        this.personaRepository.delete(persona);
    }

}
